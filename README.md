tsj 21 Jan 2020
# In this repo are raw data that are part of the paper "Data convergence in syntactic theory and the role of sentence pairs".

The stimuli can be found in list_of_items.txt, 3xx items were judged as unmarked (OK) by the LI-authors, 4xx items were judged as marked (*). 

All data from the online experiment can be found in the .xlsx, incl. user info, response times, and ratings. The various exclusion criteria are applied step by step. The sheets from userinfo_after_excl onwards are those that are post exclusion and thus reported in the paper and used for the analyses. Some data were exported to .csv so that it could be processed in R. 

The R code can be found in the corresponding .txt. all_obs.csv, online_ratings_aggregated.csv, and online_ratings_aggregated_highlight.csv are used for the R code. 